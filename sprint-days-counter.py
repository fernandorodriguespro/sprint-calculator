import sys

print('Please inform the number of Devs who will work full time: ')
total_full_devs = int(sys.stdin.readline())

print('How many Devs will work partially?')
total_partial_devs = int(sys.stdin.readline())

total_days_of_partial_devs = 0
for partial_dev in range(int(total_partial_devs)):
    print('How many days does the Dev ' + str(partial_dev + 1) + ' will apply?')
    total_days_of_partial_devs += int(sys.stdin.readline())


print('Please inform the total number of Story Points: ')
sp = sys.stdin.readline()

total_days = int(sp)/8

days_distributed = total_days / (int(total_full_devs) + int(total_partial_devs))
remaining_days_to_distribute = days_distributed - total_days_of_partial_devs

if(total_days_of_partial_devs>0):
    print("Distributing days to full time Devs...")
days_to_distribute = remaining_days_to_distribute / total_full_devs
largest_days_distributed = days_to_distribute + days_distributed

print('The maximum number of days of this project is : ' + str(round(largest_days_distributed, 2)) + ' days.')






